# Rubiks Cube
# Authors:	Gary Menezes
#		Sam Appelbaum
#		(Patrick)Yeo Ho Yoon

# A Brief Description
	A 3D rubiks cube implemented with pyOpenGL. You can jumble the cube
	and then solve it while enjoying the awesomely animated rotations!

# To Run
   Run pyGUI.py

# User Controls
    > Move selected face, also rotates cube to maintain view of selected face
	w - up
	a - left
	s - down
	d - right
    > Rotate the row/column containing the selected face
	i - up
	j - left
	k - down
	l - right
    > Jumble
	space
    > Reset Cube
	r
    > Exit
	escape
    
    **Make sure caps are off

# Gameplay
    Completion of the game is not registered immediately since the cube starts
    solved. To play, first jumble the cube and then solve it using the defined
    controls. When the cube is solved, input is locked and "YOU WIN!" is
    printed to the console. To unlock the cube and play again, either reset (r)
    or jumble (space).