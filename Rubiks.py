from numpy import *
from math import *
from face import *
from random import *

class Rubiks:
    def __init__(self):
        self.axis = array([0,0,0])
        self.facesToRotate = []
        self.totalRotation = array( [ [1,0,0,0], [0,1,0,0],[0,0,1,0],[0,0,0,1] ] )
        self.displayRotation = array( [ [1,0,0,0], [0,1,0,0],[0,0,1,0],[0,0,0,1] ] )
        self.eye = array( [0,0,1,0] )
        self.up = array( [0,1,0,0] )
        self.upCount, self.downCount, self.rightCount, self.leftCount = 1,1,1,1
        #instantiate 54 self.faces and link them up # one list holding all 54 self.faces
	self.faces = []
	self.facesByColor = []
	verts = []
	#White Side
	for y in range(3,-4,-2): # 3, 1, -1, -3 (Y value)
	    for x in range(-3,4,2): # -3, -1, 1, 3 (X value)
		verts.append(array([x,y,3]))
	wside = self.init_side(verts,array([1,1,1]))
	self.selectedFace = self.faces[4]
	#Green Side
	verts = []
	for y in range(3,-4,-2): #(Y value)
	    for z in range(3,-4,-2): #(Z value)
		verts.append(array([3,y,z]))
	gside = self.init_side(verts,array([0,1,0]))
	#Yellow Side
	verts = []
	for y in range(3,-4,-2): #(Y value)
	    for x in range(3,-4,-2): #(X value)
                verts.append(array([x,y,-3]))
	yside = self.init_side(verts,array([1,1,0]))
	#Blue Side
	verts = []
	for y in range(3,-4,-2): #(Y value)
	    for z in range(-3,4,2): #(Z value)
		verts.append(array([-3,y,z]))
	bside = self.init_side(verts,array([0,0,1]))
	#Red Side
	verts = []
	for z in range(-3,4,2): #(Z value)
	    for x in range(-3,4,2): #(X value)
		verts.append(array([x,3,z]))
	rside = self.init_side(verts,array([1,0,0]))
	#Orange Side
	verts = []
	for z in range(3,-4,-2): #(Z value)
	    for x in range(-3,4,2): #(X value)
		verts.append(array([x,-3,z]))
	oside = self.init_side(verts,array([1,.5,0]))

	
        self.mesh_left_right(wside,gside)
        self.mesh_left_right(gside,yside)
        self.mesh_left_right(yside,bside)
        self.mesh_left_right(bside,wside)
        
        #up meshing is to different sides of the top / bottom, so this part requires more parameters

        #white-red
        self.mesh_up(wside,rside,6,1,2)
        #green-red
        self.mesh_up(gside,rside,8,-3,1)
        #yellow-red
        self.mesh_up(yside,rside,2,-1,0)
        #blue-red
        self.mesh_up(bside,rside,0,3,3)

        #white-orange
        self.mesh_down(wside,oside,0,1,0)
        #green-orange
        self.mesh_down(gside,oside,2,3,1)
        #yellow-orange
        self.mesh_down(yside,oside,8,-1,2)
        #blue-orange
        self.mesh_down(bside,oside,6,-3,3)
        
        self.centers = (self.faces[4], self.faces[13], self.faces[22], self.faces[31], self.faces[40], self.faces[49])

    # 
    #   The mesh_* methods sync up the faces' pointers
    #
        
    def mesh_down(self,side1,side2,startTwo,incrTwo,directionForBottomFace):
        one,two = 6,startTwo
        for i in range(3):
            side1[one].setDown(side2[two])
            if directionForBottomFace == 0:
                side2[two].setUp(side1[one])
            elif directionForBottomFace == 1:
               side2[two].setRight(side1[one])
            elif directionForBottomFace == 2:
                side2[two].setDown(side1[one])
            elif directionForBottomFace == 3:
                side2[two].setLeft(side1[one])
            one,two = one+1,two+incrTwo
      
    def mesh_up(self,side1,side2,startTwo,incrTwo,directionForTopFace):
        one,two = 0,startTwo
        for i in range(3):
            side1[one].setUp(side2[two])
            if directionForTopFace == 0:
                side2[two].setUp(side1[one])
            elif directionForTopFace == 1:
               side2[two].setRight(side1[one])
            elif directionForTopFace == 2:
                side2[two].setDown(side1[one])
            elif directionForTopFace == 3:
                side2[two].setLeft(side1[one])
            one,two = one+1,two+incrTwo
    
    def mesh_left_right(self,side1,side2):
        one,two = 2,0
        for i in range(3):
            side1[one].setRight(side2[two])
            side2[two].setLeft(side1[one])
            one,two = one+3,two+3
    
    # 
    #   The init_side method creates and returns a side
    #
    def init_side(self,vertices,color):
	one,two,three,four = 0,4,5,1
	for i in range(3):
            self.faces.append(Face(array([vertices[one],vertices[two],vertices[three],vertices[four]]),color,False))
            one,two,three,four = one+1,two+1,three+1,four+1
            if one == 5:
                    self.faces.append(Face(array([vertices[one],vertices[two],vertices[three],vertices[four]]),color,True))
            else:
                    self.faces.append(Face(array([vertices[one],vertices[two],vertices[three],vertices[four]]),color,False))
            one,two,three,four = one+1,two+1,three+1,four+1
            self.faces.append(Face(array([vertices[one],vertices[two],vertices[three],vertices[four]]),color,False))
            one,two,three,four = one+2,two+2,three+2,four+2
        side = []
	for i in range(len(self.faces)-9,len(self.faces)):
            side.append(self.faces[i])
        side[0].setDown(side[3])
        side[0].setRight(side[1])
        side[1].setLeft(side[0])
        side[1].setDown(side[4])
        side[1].setRight(side[2])
        side[2].setLeft(side[1])
        side[2].setDown(side[5])
        
        side[3].setDown(side[6])
        side[3].setRight(side[4])
        side[3].setUp(side[0])
        side[4].setLeft(side[3])
        side[4].setDown(side[7])
        side[4].setRight(side[5])
        side[4].setUp(side[1])
        side[5].setLeft(side[4])
        side[5].setDown(side[8])
        side[5].setUp(side[2])
        
        side[6].setRight(side[7])
        side[6].setUp(side[3])
        side[7].setLeft(side[6])
        side[7].setRight(side[8])
        side[7].setUp(side[4])
        side[8].setLeft(side[7])
        side[8].setUp(side[5])
        
        return side

    # 
    #   The turn* methods, and moveSelection* methods handle input from w,a,s,d (changing face selection)
    #   The select* methods are for debugging purposes and just add a layer between the turn* and moveSelection* methods
    #
    def turnRight(self):
        self.selectRight()
        rotatedUp = dot(self.displayRotation, self.up)
        rot = self.rotation3D(rotatedUp,-30)
        #rot = self.rotation3D(array([0,1,0,0]),-30)
        self.displayRotation = dot(rot,self.displayRotation)
    def moveSelectionRight(self):
        self.selectedFace.adjacentFaces[1].updateDir(self.selectedFace,3)
        self.selectedFace = self.selectedFace.adjacentFaces[1]
    def selectRight(self):
        self.moveSelectionRight()
        self.rightCount -= 1
        self.leftCount += 1
        if self.rightCount == -1:
            rotatedUp = dot(self.totalRotation, self.up)
            self.rotate(rotatedUp,90)
            self.rightCount = 2
            self.leftCount = 0
        #gluLookAt rotation right 30 deg
    def turnLeft(self):
        self.selectLeft()
        rotatedUp = dot(self.displayRotation, self.up)
        rot = self.rotation3D(rotatedUp,30)
        #rot = self.rotation3D(array([0,1,0,0]),30)
        self.displayRotation = dot(rot,self.displayRotation)
    def moveSelectionLeft(self):
        self.selectedFace.adjacentFaces[3].updateDir(self.selectedFace,1)
        self.selectedFace = self.selectedFace.adjacentFaces[3]
    def selectLeft(self):
        self.moveSelectionLeft()
        self.leftCount -= 1
        self.rightCount += 1
        if self.leftCount == -1:
            rotatedUp = dot(self.totalRotation, self.up)
            self.rotate(rotatedUp,-90)
            self.leftCount = 2
            self.rightCount = 0
        #gluLookAt rotation left 30 deg
    def turnUp(self):
        if not self.upCount == 0:
            self.selectUp()
            rotatedEye = dot(self.displayRotation, self.eye)
            rotatedEye = array([rotatedEye[0],rotatedEye[1],rotatedEye[2]])
            rotatedUp = dot(self.displayRotation, self.up)
            rotatedUp = array([rotatedUp[0],rotatedUp[1],rotatedUp[2]])
            axis = cross(rotatedEye,rotatedUp)
            axis = array([-1,0,0,0])
            rot = self.rotation3D(axis,-30)
            self.displayRotation = dot(rot,self.displayRotation)
    def moveSelectionUp(self):
        self.selectedFace.adjacentFaces[0].updateDir(self.selectedFace,2)
        self.selectedFace = self.selectedFace.adjacentFaces[0]
    def selectUp(self):
        self.moveSelectionUp()
        self.upCount -= 1
        self.downCount += 1
    def turnDown(self):
        if not self.downCount == 0:
            self.selectDown()
            rotatedEye = dot(self.displayRotation, self.eye)
            rotatedEye = array([rotatedEye[0],rotatedEye[1],rotatedEye[2]])
            rotatedUp = dot(self.displayRotation, self.up)
            rotatedUp = array([rotatedUp[0],rotatedUp[1],rotatedUp[2]])
            axis = cross(rotatedEye,rotatedUp)
            axis = array([-1,0,0,0])
            rot = self.rotation3D(axis,30)
            self.displayRotation = dot(rot,self.displayRotation)
    def moveSelectionDown(self):
        self.selectedFace.adjacentFaces[2].updateDir(self.selectedFace,0)
        self.selectedFace = self.selectedFace.adjacentFaces[2]
    def selectDown(self):
        self.moveSelectionDown()
        self.upCount += 1
        self.downCount -= 1

    # 
    #   The rotate* methods handle rotation of sides of the cube
    #   ** These methods aren't very well written - specifically, they contain a lot of repetitive code between them
    #
    def rotate(self, axis, angle):
        rot = self.rotation3D(axis,angle)
        self.totalRotation = dot(rot,self.totalRotation)
    def rotation3D(self,axis,angle):
        angleRad = angle * pi / 180.0
        c = cos(angleRad)
        s = sin(angleRad)
        t = 1.0 - c
        length = sqrt(axis[0]*axis[0]+axis[1]*axis[1]+axis[2]*axis[2])
        for x in axis:
            x /=length
        rot =  array ([[t * axis[0] * axis[0] + c, t * axis[0] * axis[1] - s * axis[2],
             t * axis[0] * axis[2] + s * axis[1],0.0], 
             [t * axis[0] * axis[1] + s * axis[2], 
              t * axis[1] * axis[1] + c, t * axis[1] * axis[2] - s * axis[0],0.0],
              [t * axis[0] * axis[2] - s * axis[1],t * axis[1] * axis[2] + s * axis[0],
             t * axis[2] * axis[2] + c, 0.0],
             [0.0, 0.0, 0.0, 1.0]])
        return rot
    def rotateRight(self,animate):
        self.facesToRotate = []
        tempRightCount = self.rightCount
        tempLeftCount = self.leftCount
        start = self.selectedFace
        curr = start
        while True:
            curr.adjacentFaces[1].updateDir(curr,3)
            curr = curr.adjacentFaces[1]
            if curr==start:
                break     
        up = [start.adjacentFaces[0],start.adjacentFaces[1].adjacentFaces[0],start.adjacentFaces[1].adjacentFaces[1].adjacentFaces[0]]
        down = [start.adjacentFaces[2],start.adjacentFaces[1].adjacentFaces[2],start.adjacentFaces[1].adjacentFaces[1].adjacentFaces[2]]
        next = curr.adjacentFaces[1].adjacentFaces[1].adjacentFaces[1]
        curr.adjacentFaces[1].updateDir(curr,3)
        curr.adjacentFaces[1].adjacentFaces[1].updateDir(curr.adjacentFaces[1],3)
        next.updateDir(curr.adjacentFaces[1].adjacentFaces[1],3)
        self.axis = dot(self.totalRotation,self.up)
        currRotatedNormal = dot(curr.transMatrix, curr.normal())
        ignoreSide = -1
        for x in range(0,3,2):
            i = curr.adjacentFaces[x]
            rotatedNormal = dot(i.transMatrix, i.normal())
            if not abs(rotatedNormal[0] - currRotatedNormal[0]) < .1 or not abs(rotatedNormal[1] - currRotatedNormal[1]) < .1 or not abs(rotatedNormal[2] - currRotatedNormal[2]) < .1:
                side = self.getSide(i)
                ignoreSide = x
                for j in side:
                    self.facesToRotate.append(j)
                    if not animate:
                        j.rotate(self.axis,90);
        while True:
            if next == start:
                if not ignoreSide == 0:
                    up[0].replace(next,curr)
                    curr.setUp(up[0])
                if not ignoreSide == 2:
                    down[0].replace(next,curr)
                    curr.setDown(down[0])
            elif next == start.adjacentFaces[1]:
                if not ignoreSide == 0:
                    up[1].replace(next,curr)
                    curr.setUp(up[1])
                if not ignoreSide == 2:
                    down[1].replace(next,curr)
                    curr.setDown(down[1])
            elif next == start.adjacentFaces[1].adjacentFaces[1]:
                if not ignoreSide == 0:
                    up[2].replace(next,curr)
                    curr.setUp(up[2])
                if not ignoreSide == 2:
                    down[2].replace(next,curr)
                    curr.setDown(down[2])
            else:
                if not ignoreSide == 0:
                    next.adjacentFaces[0].replace(next,curr)
                    curr.setUp(next.adjacentFaces[0])
                if not ignoreSide == 2:
                    next.adjacentFaces[2].replace(next,curr)
                    curr.setDown(next.adjacentFaces[2])
            self.facesToRotate.append(curr)
            if not animate:
                curr.rotate(self.axis,90)
            self.selectRight()
            curr = self.selectedFace
            next = curr.adjacentFaces[1].adjacentFaces[1].adjacentFaces[1]
            curr.adjacentFaces[1].updateDir(curr,3)
            curr.adjacentFaces[1].adjacentFaces[1].updateDir(curr.adjacentFaces[1],3)
            next.updateDir(curr.adjacentFaces[1].adjacentFaces[1],3)
            if curr==start:
                break
        for i in range(3):
            self.moveSelectionLeft()
        self.rightCount = tempRightCount
        self.leftCount = tempLeftCount
        
    def rotateLeft(self,animate):
        self.facesToRotate = []
        tempRightCount = self.rightCount
        tempLeftCount = self.leftCount
        start = self.selectedFace
        curr = start
        while True:
            curr.adjacentFaces[3].updateDir(curr,1)
            curr = curr.adjacentFaces[3]
            if curr==start:
                break
        up = [start.adjacentFaces[0],start.adjacentFaces[3].adjacentFaces[0],start.adjacentFaces[3].adjacentFaces[3].adjacentFaces[0]]
        down = [start.adjacentFaces[2],start.adjacentFaces[3].adjacentFaces[2],start.adjacentFaces[3].adjacentFaces[3].adjacentFaces[2]]
        next = curr.adjacentFaces[3].adjacentFaces[3].adjacentFaces[3]
        curr.adjacentFaces[3].updateDir(curr,1)
        curr.adjacentFaces[3].adjacentFaces[3].updateDir(curr.adjacentFaces[3],1)
        next.updateDir(curr.adjacentFaces[3].adjacentFaces[3],1)
        self.axis = dot(self.totalRotation, self.up)
        currRotatedNormal = dot(curr.transMatrix, curr.normal())
        ignoreSide = -1
        for x in range(0,3,2):
            i = curr.adjacentFaces[x]
            rotatedNormal = dot(i.transMatrix, i.normal())
            if not abs(rotatedNormal[0] - currRotatedNormal[0]) < .1 or not abs(rotatedNormal[1] - currRotatedNormal[1]) < .1 or not abs(rotatedNormal[2] - currRotatedNormal[2]) < .1:
                side = self.getSide(i)
                ignoreSide = x
                for j in side:
                    self.facesToRotate.append(j)
                    if not animate:
                        j.rotate(self.axis,-90);
        while True:
            if next == start:
                if not ignoreSide == 0:
                    up[0].replace(next,curr)
                    curr.setUp(up[0])
                if not ignoreSide == 2:
                    down[0].replace(next,curr)
                    curr.setDown(down[0])
            elif next == start.adjacentFaces[3]:
                if not ignoreSide == 0:
                    up[1].replace(next,curr)
                    curr.setUp(up[1])
                if not ignoreSide == 2:
                    down[1].replace(next,curr)
                    curr.setDown(down[1])
            elif next == start.adjacentFaces[3].adjacentFaces[3]:
                if not ignoreSide == 0:
                    up[2].replace(next,curr)
                    curr.setUp(up[2])
                if not ignoreSide == 2:
                    down[2].replace(next,curr)
                    curr.setDown(down[2])
            else:
                if not ignoreSide == 0:
                    next.adjacentFaces[0].replace(next,curr)
                    curr.setUp(next.adjacentFaces[0])
                if not ignoreSide == 2:
                    next.adjacentFaces[2].replace(next,curr)
                    curr.setDown(next.adjacentFaces[2])
            self.facesToRotate.append(curr)
            if not animate:
                curr.rotate(self.axis,-90)
            self.selectLeft()
            curr = self.selectedFace
            next = curr.adjacentFaces[3].adjacentFaces[3].adjacentFaces[3]
            curr.adjacentFaces[3].updateDir(curr,1)
            curr.adjacentFaces[3].adjacentFaces[3].updateDir(curr.adjacentFaces[3],1)
            next.updateDir(curr.adjacentFaces[3].adjacentFaces[3],1)
            if curr==start:
                break
        for i in range(3):
            self.moveSelectionRight()
        self.rightCount = tempRightCount
        self.leftCount = tempLeftCount
    def rotateUp(self,animate):
        self.facesToRotate = []
        tempUpCount = self.upCount
        tempDownCount = self.downCount
        start = self.selectedFace
        curr = start
        while True:
            curr.adjacentFaces[0].updateDir(curr,2)
            curr = curr.adjacentFaces[0]
            if curr==start:
                break    
        right = [start.adjacentFaces[1],start.adjacentFaces[0].adjacentFaces[1],start.adjacentFaces[0].adjacentFaces[0].adjacentFaces[1]]
        left = [start.adjacentFaces[3],start.adjacentFaces[0].adjacentFaces[3],start.adjacentFaces[0].adjacentFaces[0].adjacentFaces[3]]
        next = curr.adjacentFaces[0].adjacentFaces[0].adjacentFaces[0]
        curr.adjacentFaces[0].updateDir(curr,2)
        curr.adjacentFaces[0].adjacentFaces[0].updateDir(curr.adjacentFaces[0],2)
        next.updateDir(curr.adjacentFaces[0].adjacentFaces[0],2)
        rotatedEye = dot(self.totalRotation, self.eye)
        rotatedEye = array([rotatedEye[0],rotatedEye[1],rotatedEye[2]])
        rotatedUp = dot(self.totalRotation, self.up)
        rotatedUp = array([rotatedUp[0],rotatedUp[1],rotatedUp[2]])
        self.axis = cross(rotatedEye,rotatedUp)
        currRotatedNormal = dot(curr.transMatrix, curr.normal())
        ignoreSide = -1
        for x in range(1,4,2):
            i = curr.adjacentFaces[x]
            rotatedNormal = dot(i.transMatrix, i.normal())
            if not abs(rotatedNormal[0] - currRotatedNormal[0]) < .1 or not abs(rotatedNormal[1] - currRotatedNormal[1]) < .1 or not abs(rotatedNormal[2] - currRotatedNormal[2]) < .1:
                side = self.getSide(i)
                ignoreSide = x
                for j in side:
                    self.facesToRotate.append(j)
                    if not animate:
                        j.rotate(self.axis,90);
        while True:
            if next == start:
                if not ignoreSide == 1:
                    right[0].replace(next,curr)
                    curr.setRight(right[0])
                if not ignoreSide == 3:
                    left[0].replace(next,curr)
                    curr.setLeft(left[0])
            elif next == start.adjacentFaces[0]:
                if not ignoreSide == 1:
                    right[1].replace(next,curr)
                    curr.setRight(right[1])
                if not ignoreSide == 3:
                    left[1].replace(next,curr)
                    curr.setLeft(left[1])
            elif next == start.adjacentFaces[0].adjacentFaces[0]:
                if not ignoreSide == 1:
                    right[2].replace(next,curr)
                    curr.setRight(right[2])
                if not ignoreSide == 3:
                    left[2].replace(next,curr)
                    curr.setLeft(left[2])
            else:
                if not ignoreSide == 1:
                    next.adjacentFaces[1].replace(next,curr)
                    curr.setRight(next.adjacentFaces[1])
                if not ignoreSide == 3:
                    next.adjacentFaces[3].replace(next,curr)
                    curr.setLeft(next.adjacentFaces[3])
            self.facesToRotate.append(curr)
            if not animate:
                curr.rotate(self.axis,90)
            self.selectUp()
            curr = self.selectedFace
            next = curr.adjacentFaces[0].adjacentFaces[0].adjacentFaces[0]
            curr.adjacentFaces[0].updateDir(curr,2)
            curr.adjacentFaces[0].adjacentFaces[0].updateDir(curr.adjacentFaces[0],2)
            next.updateDir(curr.adjacentFaces[0].adjacentFaces[0],2)
            if curr==start:
                break
        for i in range(3):
            self.moveSelectionDown()
        self.upCount = tempUpCount
        self.downCount = tempDownCount

    def rotateDown(self,animate):
        self.facesToRotate = []
        tempUpCount = self.upCount
        tempDownCount = self.downCount
        start = self.selectedFace
        curr = start
        while True:
            curr.adjacentFaces[2].updateDir(curr,0)
            curr = curr.adjacentFaces[2]
            if curr==start:
                break  
        right = [start.adjacentFaces[1],start.adjacentFaces[2].adjacentFaces[1],start.adjacentFaces[2].adjacentFaces[2].adjacentFaces[1]]
        left = [start.adjacentFaces[3],start.adjacentFaces[02].adjacentFaces[3],start.adjacentFaces[2].adjacentFaces[2].adjacentFaces[3]]
        next = curr.adjacentFaces[2].adjacentFaces[2].adjacentFaces[2]
        curr.adjacentFaces[2].updateDir(curr,0)
        curr.adjacentFaces[2].adjacentFaces[2].updateDir(curr.adjacentFaces[2],0)
        next.updateDir(curr.adjacentFaces[2].adjacentFaces[2],0)
        rotatedEye = dot(self.totalRotation, self.eye)
        rotatedEye = array([rotatedEye[0],rotatedEye[1],rotatedEye[2]])
        rotatedUp = dot(self.totalRotation, self.up)
        rotatedUp = array([rotatedUp[0],rotatedUp[1],rotatedUp[2]])
        self.axis = cross(rotatedEye,rotatedUp)
        currRotatedNormal = dot(curr.transMatrix, curr.normal())
        ignoreSide = -1
        for x in range(1,4,2):
            i = curr.adjacentFaces[x]
            rotatedNormal = dot(i.transMatrix, i.normal())
            if not abs(rotatedNormal[0] - currRotatedNormal[0]) < .1 or not abs(rotatedNormal[1] - currRotatedNormal[1]) < .1 or not abs(rotatedNormal[2] - currRotatedNormal[2]) < .1:
                side = self.getSide(i)
                ignoreSide = x
                for j in side:
                    self.facesToRotate.append(j)
                    if not animate:
                        j.rotate(self.axis,-90);
        while True:
            if next == start:
                if not ignoreSide == 1:
                    right[0].replace(next,curr)
                    curr.setRight(right[0])
                if not ignoreSide == 3:
                    left[0].replace(next,curr)
                    curr.setLeft(left[0])
            elif next == start.adjacentFaces[2]:
                if not ignoreSide == 1:
                    right[1].replace(next,curr)
                    curr.setRight(right[1])
                if not ignoreSide == 3:
                    left[1].replace(next,curr)
                    curr.setLeft(left[1])
            elif next == start.adjacentFaces[2].adjacentFaces[2]:
                if not ignoreSide == 1:
                    right[2].replace(next,curr)
                    curr.setRight(right[2])
                if not ignoreSide == 3:
                    left[2].replace(next,curr)
                    curr.setLeft(left[2])
            else:
                if not ignoreSide == 1:
                    next.adjacentFaces[1].replace(next,curr)
                    curr.setRight(next.adjacentFaces[1])
                if not ignoreSide == 3:
                    next.adjacentFaces[3].replace(next,curr)
                    curr.setLeft(next.adjacentFaces[3])
            self.facesToRotate.append(curr)
            if not animate:
                curr.rotate(self.axis,-90)
            self.selectDown()
            curr = self.selectedFace
            next = curr.adjacentFaces[2].adjacentFaces[2].adjacentFaces[2]
            curr.adjacentFaces[2].updateDir(curr,0)
            curr.adjacentFaces[2].adjacentFaces[2].updateDir(curr.adjacentFaces[2],0)
            next.updateDir(curr.adjacentFaces[2].adjacentFaces[2],0)
            if curr==start:
                break
        for i in range(3):
            self.moveSelectionUp()
        self.upCount = tempUpCount
        self.downCount = tempDownCount

    #returns a list of all the faces on the same side as this face
    def getSide(self, face):
        facesInSide = []
        currRotatedNormal = dot(face.transMatrix, face.normal())
        for i in self.faces:
            if not i == face:
                rotatedNormal = dot(i.transMatrix, i.normal())
                if abs(rotatedNormal[0] - currRotatedNormal[0]) < .1 and abs(rotatedNormal[1] - currRotatedNormal[1]) < .1 and abs(rotatedNormal[2] - currRotatedNormal[2]) < .1:
                    facesInSide.append(i)
        facesInSide.append(face)
        return facesInSide
    def getCube(self):
        #return list of all 54 faces
	return self.faces
    def getSelected(self):
        #returning type Face
        return self.selectedFace
    
    def isFinished(self):
        for f in self.centers:
            if  f.isCenter:
                color = f.color
                f.adjacentFaces[0].updateDir(f,2)
                f.adjacentFaces[2].updateDir(f,0)
                faces =  [f.adjacentFaces[0], f.adjacentFaces[1], f.adjacentFaces[2], f.adjacentFaces[3], f.adjacentFaces[0].adjacentFaces[3], f.adjacentFaces[0].adjacentFaces[1], f.adjacentFaces[2].adjacentFaces[3], f.adjacentFaces[2].adjacentFaces[1]]
                for x in faces:
                    if not x.color[0] == color[0] or not x.color[1] == color[1] or not x.color[2] == color[2]:
                        return False
        return True
    
    def jumble(self):
        numJumble = 2
        rotateMap = {0:"rotateUp", 1:"rotateRight", 2:"rotateDown", 3:"rotateLeft"}
        selectMap = {0:"turnUp", 1:"turnRight", 2:"turnDown", 3:"turnLeft"}
        for i in range(numJumble):
            rand = randint(0,3)
            exec("self."+selectMap[rand]+"()")
            rand = randint(0,3)
            exec("self."+rotateMap[rand]+"(False)")
        while not self.selectedFace.isCenter():
            rand = randint(0,3)
            exec("self."+selectMap[rand]+"()")
            rand = randint(0,3)
            exec("self."+rotateMap[rand]+"(False)")
