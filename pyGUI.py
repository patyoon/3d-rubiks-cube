#!/usr/bin/env python



from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from numpy import *
import sys
import face
from Rubiks import *
import time

# for some reason glut likes hex IDs for escape...
# this is equivalent to ESCAPE = 27...I think.
# regardless, this works.
ESCAPE = '\x1b'

# Number of the glut window.
window = 0


# Rotation angle
rotDegree = 30;

#selected square
sel=None

checkWin = False
gameOver = False

cube=Rubiks()

# initialize stuff....
def initGL(Width, Height):
    glClearColor(0.0, 0.0, 0.0, 0.0)	# BG color is black
    glClearDepth(1.0)					
    glDepthFunc(GL_LESS)
    glEnable(GL_DEPTH_TEST)

    #Don't know why I do this...but 277 taught me this works
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()					
    #find aspect ratio
    gluPerspective(45.0, float(Width)/float(Height), 0.1, 100.0)
    #Don't know why I do this either...but 277 taught me this works as well
    glMatrixMode(GL_MODELVIEW)

# call when resizes window
def resizeGL(Width, Height):
    if Height == 0:				#prevent division by 0 	
	    Height = 1

    glViewport(0, 0, Width, Height)		#reset viewport
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(Width)/float(Height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)
    
def animate(axis, angle, list):
        increment = 1
        if angle < 0:
            increment = -1
        angle = abs(angle)
        start = 0
        end = angle
        FPS = 1000
        last_time = time.time()
        while start<end:
            # draw animation
            for face in list:
                face.rotate(axis, increment)
            new_time = time.time()
            sleep_time = ((1000.0 / FPS) - (new_time - last_time)) / 1000.0
            if sleep_time > 0:
                time.sleep(sleep_time)
            last_time = new_time
            start+=1
            draw()
            
#main draw method
def draw():
    #force global so it won't die
    global sel
    sel=cube.getSelected()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); #clear screen & depth buf.
    glLoadIdentity();                   #reset view
    
    glTranslatef(0.0,0.0,-20)

    #self.animate(axis,90,facesToRotate)
    
    glBegin(GL_QUADS)           #draw cube things
    faces = cube.getCube()#[:9]
    for face in faces:
        glColor3f(face.color[0],face.color[1],face.color[2])
        for vertex in face.vertexList:
            temp = array([vertex[0],vertex[1],vertex[2],1])
            transformedVertex = dot(cube.displayRotation,dot(face.getTransformation(),temp))
            glVertex3f(transformedVertex[0],transformedVertex[1],transformedVertex[2])

    glEnd()		#done making quads!!!


    #outline selected face in purple (for now)
    glLineWidth(10)
    glBegin(GL_LINE_STRIP)
    for vert in sel.vertexList:
        temp = array([vert[0],vert[1],vert[2],1])
        rotated = dot(cube.displayRotation,dot(sel.getTransformation(),temp))
        glColor3f(1,0,.5)
        glVertex3f(rotated[0]*1.02,rotated[1]*1.02,rotated[2]*1.02)
        normal = face.normal()
        glNormal3f(normal[0],normal[1],normal[2])
    temp = array([sel.vertexList[0][0],sel.vertexList[0][1],sel.vertexList[0][2],1])
    rotated = dot(cube.displayRotation,dot(sel.getTransformation(),temp))
    glVertex3f(rotated[0]*1.02,rotated[1]*1.02,rotated[2]*1.02)
    glEnd()

    #outline all other faces in black
    for face in cube.getCube():
        if not face == sel:
            glBegin(GL_LINE_STRIP)
            for vert in face.vertexList:
                temp = array([vert[0],vert[1],vert[2],1])
                rotated = dot(cube.displayRotation,dot(face.getTransformation(),temp))
                glColor3f(0,0,0)
                glVertex3f(rotated[0]*1.02,rotated[1]*1.02,rotated[2]*1.02)
            temp = array([face.vertexList[0][0],face.vertexList[0][1],face.vertexList[0][2],1])
            rotated = dot(cube.displayRotation,dot(face.getTransformation(),temp))
            glVertex3f(rotated[0]*1.02,rotated[1]*1.02,rotated[2]*1.02)
            glEnd()
    
    #since this is double buffered, swap the buffers to display what just got drawn. 
    glutSwapBuffers()

#args is a tuple...
#called when key is pressed
def keyPressed(*args):
    global cube, checkWin, gameOver
    # If escape is pressed, quit
    if args[0] == ESCAPE:
        glutDestroyWindow(window)
        sys.exit(0)
    if not gameOver:
        if args[0] == "f":
            cube.selectLeft()
        elif args[0] == "g":
            cube.selectDown()
        elif args[0] == "h":
            cube.selectRight()
        elif args[0] == "t":
            cube.selectUp()
        elif args[0] == "a":
            cube.turnLeft()
        elif args[0] == "s":
            cube.turnDown()
        elif args[0] == "d":
            cube.turnRight()
        elif args[0] == "w":
            cube.turnUp()
        elif args[0] == "j":
            cube.rotateLeft(True)
            animate(cube.axis,-90,cube.facesToRotate)
            checkWin = True
        elif args[0] == "k":
            cube.rotateDown(True)
            animate(cube.axis,-90,cube.facesToRotate)
            checkWin = True
        elif args[0] == "l":
            cube.rotateRight(True)
            animate(cube.axis,90,cube.facesToRotate)
            checkWin = True
        elif args[0] == "i":
            cube.rotateUp(True)
            animate(cube.axis,90,cube.facesToRotate)
            checkWin = True
    if args[0] == "r":
        cube = Rubiks()
        checkWin = False
        gameOver = False
    if args[0] == " ":
        cube.jumble()
        checkWin = True
        gameOver = False
    if not gameOver and checkWin and cube.isFinished():
        print "YOU WIN!"
        gameOver = True

def main():
	global window

	glutInit(sys.argv)

	#display modes enabled:   
	#rgba
	#2x buffer
	#alpha supported 
	#depth buffer
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ACCUM | GLUT_ALPHA | GLUT_DEPTH)
	
	#make window
	glutInitWindowSize(800, 600)
	glutInitWindowPosition(0, 0)
	
	window = glutCreateWindow("SUPER pyRubik")

        #register draw method in glut
	glutDisplayFunc(draw)

	#when idle (hehe) draw.
	glutIdleFunc(draw)
		
	#register resize method
	glutReshapeFunc(resizeGL)
	
	
	#register method called on keyboard button press  
	glutKeyboardFunc(keyPressed)
	
	#init!
	initGL(800, 600)


if __name__ == '__main__':
	main()
	glutMainLoop()
    	
