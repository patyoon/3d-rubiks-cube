'''
Created on 2010. 5. 8.

@author: Patrick
'''
from time import strftime, gmtime
from wx.glcanvas import GLCanvas
import wx

from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys,math

from numpy import *
import sys
import face
from Rubiks import *
import time

# for some reason glut likes hex IDs for escape...
# this is equivalent to ESCAPE = 27...I think.
# regardless, this works.
ESCAPE = '\x1b'

# Number of the glut window.
window = 0


# Rotation angle
rotDegree = 30;

#selected square
sel=None

checkWin = False
gameOver = False

Width = 800
Height = 600

cube=Rubiks()


class myGLCanvas(GLCanvas):
    
    numGame = 1
    
    def __init__(self, parent):
        GLCanvas.__init__(self, parent,-1)
        wx.EVT_PAINT(self, self.OnPaint)
        self.init = 0
        #wx.EVT_WINDOW_DESTROY(self, self.onDestroy)
        wx.EVT_SIZE(self, self.resizeGL)
        parent.Bind(wx.EVT_KEY_DOWN, self.keyPressed)
        wx.EVT_KEY_DOWN(self, self.keyPressed)
        parent.SetFocus()
        self.label2 = wx.StaticText(self, -1, "Game Number:"+str(myGLCanvas.numGame), wx.Point(0, 0), size= wx.Size(100, 100))
        font1 = wx.Font(30, wx.ROMAN, wx.NORMAL, wx.NORMAL, False, u'Verdana')
        self.label2.SetBackgroundColour("black")
        self.label2.SetForegroundColour("white")
        self.label2.SetFont(font1)

    def resizeGL(self, event):
        try:
            width, height = event.GetSize()
        except:
            width = event.GetSize().width
            height = event.GetSize().height
        wx.EVT_KEY_DOWN(self, self.keyPressed)
        self.Refresh()
        self.Update()
        self.draw()
    
    def animate(self, axis, angle, list):
        increment = 1
        if angle < 0:
            increment = -1
        angle = abs(angle)
        start = 0
        end = angle
        FPS = 1000
        last_time = time.time()
        while start<end:
            # draw animation
            for face in list:
                face.rotate(axis, increment)
            new_time = time.time()
            sleep_time = ((1000.0 / FPS) - (new_time - last_time)) / 1000.0
            if sleep_time > 0:
                time.sleep(sleep_time)
            last_time = new_time
            start+=1
            self.draw()
    
    def onDestroy(self, event):
        print "Destroy Window"
    
    def OnPaint(self,event):
        dc = wx.PaintDC(self)
        self.SetCurrent()
        if not self.init:
            self.InitGL()
            self.init = 1
            self.draw()
        return

    def draw(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()
        glPushMatrix()
        color = [1.0,0.,0.,1.]
        #glMaterialfv(GL_FRONT,GL_DIFFUSE,color)
        
        glTranslatef(0.0,0.0,-20)
    
        #self.animate(axis,90,facesToRotate)
        
        glBegin(GL_QUADS)           #draw cube things
        
        global sel
        sel=cube.getSelected()
        
        faces = cube.getCube()#[:9]
        for face in faces:
            glColor3f(face.color[0],face.color[1],face.color[2])
            for vertex in face.vertexList:
                temp = array([vertex[0],vertex[1],vertex[2],1])
                transformedVertex = dot(cube.displayRotation,dot(face.getTransformation(),temp))
                glVertex3f(transformedVertex[0],transformedVertex[1],transformedVertex[2])
    
        glEnd()        #done making quads!!!
    
    
        #outline selected face in purple (for now)
        glLineWidth(10)
        glBegin(GL_LINE_STRIP)
        for vert in sel.vertexList:
            temp = array([vert[0],vert[1],vert[2],1])
            rotated = dot(cube.displayRotation,dot(sel.getTransformation(),temp))
            glColor3f(1,0,.5)
            glVertex3f(rotated[0]*1.02,rotated[1]*1.02,rotated[2]*1.02)
            normal = face.normal()
            glNormal3f(normal[0],normal[1],normal[2])
        temp = array([sel.vertexList[0][0],sel.vertexList[0][1],sel.vertexList[0][2],1])
        rotated = dot(cube.displayRotation,dot(sel.getTransformation(),temp))
        glVertex3f(rotated[0]*1.02,rotated[1]*1.02,rotated[2]*1.02)
        glEnd()
    
        #outline all other faces in black
        for face in cube.getCube():
            if not face == sel:
                glBegin(GL_LINE_STRIP)
                for vert in face.vertexList:
                    temp = array([vert[0],vert[1],vert[2],1])
                    rotated = dot(cube.displayRotation,dot(face.getTransformation(),temp))
                    glColor3f(0,0,0)
                    glVertex3f(rotated[0]*1.02,rotated[1]*1.02,rotated[2]*1.02)
                temp = array([face.vertexList[0][0],face.vertexList[0][1],face.vertexList[0][2],1])
                rotated = dot(cube.displayRotation,dot(face.getTransformation(),temp))
                glVertex3f(rotated[0]*1.02,rotated[1]*1.02,rotated[2]*1.02)
                glEnd()
        
        glPopMatrix()
        self.SwapBuffers()
        return
    
    def InitGL(self):
        # set viewing projection
        glEnable(GL_DEPTH_TEST)
        glClearColor(0.0, 0.0, 0.0, 1.0)
        glClearDepth(1.0)

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45.0, float(Width)/float(Height), 0.1, 100.0)

        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        gluLookAt(0.0, 0.0, 10.0,
                  0.0, 0.0, 0.0,
                  0.0, 1.0, 0.0)
        return
    
    def keyPressed(self, event):
        
        keycode = event.GetKeyCode()
        global cube, checkWin, gameOver
        # If escape is pressed, quit
        if keycode == wx.WXK_ESCAPE:
            glutDestroyWindow(window)
            sys.exit(0)
        if not gameOver:
            
            if keycode == wx.WXK_LEFT:
                cube.turnLeft()
            elif keycode == wx.WXK_DOWN:
                cube.turnDown()
            elif keycode == wx.WXK_RIGHT:
                cube.turnRight()
            elif keycode == wx.WXK_UP:
                cube.turnUp()
            elif keycode == wx.WXK_DELETE:
                cube.rotateLeft(True)
                self.animate(cube.axis,-90,cube.facesToRotate)
                checkWin = True
            elif keycode == wx.WXK_END:
                cube.rotateDown(True)
                self.animate(cube.axis,-90,cube.facesToRotate)
                checkWin = True
            elif keycode == wx.WXK_PAGEDOWN:
                cube.rotateRight(True)
                self.animate(cube.axis,90,cube.facesToRotate)
                checkWin = True
            elif keycode == wx.WXK_HOME:
                cube.rotateUp(True)
                self.animate(cube.axis,90,cube.facesToRotate)
                checkWin = True
        if keycode == wx.WXK_BACK:
            myGLCanvas.numGame+=1
            self.label2.SetLabel("Game Number:"+str(myGLCanvas.numGame))
            cube = Rubiks()
            checkWin = False
            gameOver = False
        if keycode == wx.WXK_SPACE:
            cube.jumble()
            checkWin = True
            gameOver = False
        if not gameOver and checkWin and cube.isFinished():
            self.label2.SetLabel("YOU WIN!")
            gameOver = True
            
        self.draw()
    
        

def main():
    app = wx.App()
    frame = wx.Frame(None,-1,'SUPER pyRubik',wx.DefaultPosition,wx.Size(800,600))
    #MyPanel(frame, -1)
    canvas = myGLCanvas(frame)
    frame.Show()
    app.MainLoop()  

if __name__ == '__main__': main()
