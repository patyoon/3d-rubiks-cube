# Rubiks Cube
# Authors:	Gary Menezes
#		Sam Appelbaum
#		(Patrick)Yeo Ho Yoon

# A Brief Description
	A 3D rubiks cube implemented with pyOpenGL. You can jumble the cube
	and then solve it while enjoying the awesomely animated rotations!

# User Controls
    > Move selected face, also rotates cube to maintain view of selected face
	up - up
	left - left
	left - down
	left - right
    > Rotate the row/column containing the selected face
	home - up
	delete - left
	end - down
	page down - right
    > Jumble
	space
    > Reset Cube
	backspace
    
    **Make sure caps are off

# Gameplay
    Completion of the game is not registered immediately since the cube starts
    solved. To play, first jumble the cube and then solve it using the defined
    controls. When the cube is solved, input is locked and "YOU WIN!" is
    printed to the console. To unlock the cube and play again, either reset (r)
    or jumble (space).