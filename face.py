'''
Created on 2010. 4. 15.

@author: Patrick
'''

from numpy import *
from math import pi
from math import cos
from math import sin
from math import sqrt

class Face:
    
    numFace = 0
    #constructor
    def __init__(self, vertices, color, center):
        self.vertexList = vertices
        self.color = color
        self.center = center
        self.transMatrix = array( [ [1,0,0,0], [0,1,0,0],[0,0,1,0],[0,0,0,1] ] ) 
        self.adjacentFaces = [None, None, None, None]
        Face.numFace +=1
        self.id = Face.numFace
    #returns the normal vector to this face
    def normal(self):
        v1 = self.vertexList[0]
        v2 = self.vertexList[1]
        v3 = self.vertexList[2]
        dv1 = v1-v2
        dv2 = v3-v2
        arr = cross(dv1,dv2)
        return array([arr[0],arr[1],arr[2],0])
    #updates the direction of the "face" to be "direction" 
    #and corrects the direction of other three adjacent faces based on new direction of "face".
    def updateDir(self,face, direction):
        found = -1
        for i in range(len(self.adjacentFaces)):
            if self.adjacentFaces[i] == face:
                found = i
        if found <0:
            raise RuntimeError('updateDir: could not update. Face not found')
        list = [None, None, None, None]
        for i in range(len(self.adjacentFaces)):
            list[(direction-found+i)%4] = self.adjacentFaces[i]
        self.adjacentFaces = list
    
    #replace adjacent face face1 with face2
    def replace(self, face1, face2):
        x = -1
        for i in range(len(self.adjacentFaces)):
            if self.adjacentFaces[i] == face1:
                x = i
        if x<0:
            raise RuntimeError('replace: could not replace. Face not found')
        else:
            self.adjacentFaces[x] = face2
      
    #returns its current transformation matrix.(2d numpy array)
    def getTransformation(self):
        return self.transMatrix
    
    #sets argument face as the up face of this object
    def setUp(self, face):
        self.adjacentFaces[0] = face
         
    #sets argument face as the right face of this object
    def setRight(self, face):
        self.adjacentFaces[1] = face
        
    #sets argument face as the down face of this object
    def setDown(self, face):
        self.adjacentFaces[2] = face
    
    #sets argument face as the left face of this object
    def setLeft(self, face):
        self.adjacentFaces[3] = face        
        
    #returns true if this is at the center of a side of Rubik's cube
    def isCenter(self):
        return self.center
    
    # Apply rotation to transformation matrix
    # Axis will be given as 4d vector(Numpy array of size 4). angle is the angle of rotation in degrees
    def rotate(self, axis, angle):
        angleRad = angle * pi / 180.0
        c = cos(angleRad)
        s = sin(angleRad)
        t = 1.0 - c
        length = sqrt(axis[0]*axis[0]+axis[1]*axis[1]+axis[2]*axis[2])
        for x in axis:
            x /=length
        rot =  array ([[t * axis[0] * axis[0] + c, t * axis[0] * axis[1] - s * axis[2],
             t * axis[0] * axis[2] + s * axis[1],0.0], 
             [t * axis[0] * axis[1] + s * axis[2], 
              t * axis[1] * axis[1] + c, t * axis[1] * axis[2] - s * axis[0],0.0],
              [t * axis[0] * axis[2] - s * axis[1],t * axis[1] * axis[2] + s * axis[0],
             t * axis[2] * axis[2] + c, 0.0],
             [0.0, 0.0, 0.0, 1.0]])
        self.transMatrix = dot(rot,self.transMatrix)
    
    #returns the id of the face
    def getID(self):
        return self.id
        
